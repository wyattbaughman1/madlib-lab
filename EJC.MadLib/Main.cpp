
// MadLib
// Wyatt Baughman

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

//Prototype
void MadLibPrint(string MadLib[]);

int main()
{
	
	//St. Patricks Themed Mad Lib -- https://www.woojr.com/st-patricks-day-ad-libs-for-kids/st-patricks-wear-green/

	string MadLib[16]; //Starts at zero otherwise would be 16 total.
	string arrMadlib[16] = {
		"Month",
		"verb (-ing)",
		"color",
		"adjective",
		"noun",
		"color",
		"adjective",
		"adverb",
		"plural noun",
		"article of clothing",
		"verb",
		"color",
		"adjective",
		"number",
		"noun",
		"adjective"
	};

	for (int i = 0; i < 16; i++)
	{
		cout << "Enter a " << arrMadlib[i] << "\n";
		getline(cin, MadLib[i]);
	}

	MadLibPrint(MadLib);

	//sending to a text file.

	string path = "madlib.txt";

	string y;

	cout << "Would you like to save output to file? " << "(y/n):"; cin >> y;


	if (y == "y" || y == "Y")
	{
		cout << "Mad Lib is being saved to madlib.txt.\n";

		ofstream ofs(path);
		
			ofs << "Every year in " << MadLib[0] << ", people celebrate Irish heritage by ";

			ofs << MadLib[1] << " the color " << MadLib[2] << ". ";

			ofs << "Some people wear " << MadLib[3] << " hats with a " << MadLib[4];

			ofs << " on them. Others wear " << MadLib[5] << " " << MadLib[6] << " ties, or ";

			ofs << MadLib[7] << " colored " << MadLib[8] << " " << MadLib[9] << "." << " and pins that say \"" << MadLib[10];

			ofs << " me, Im Irish!\" " << "are popular. The color " << MadLib[11] << " is associated with St.Patricks Day partly because Irelands nickname is The " << MadLib[12] << " Isle, and also because of the \"";

			ofs << MadLib[13] << " leaf " << MadLib[14] << ", a symbol of " << MadLib[15] << " luck.\n";

		ofs.close();
	}
	cout << "Press any key to exit...";


	(void)_getch();
	return 0;
}


void MadLibPrint(string MadLib[])
{
	//Print the array with the Madlib message here // ex: cout << MadLib[1];

	
	
	cout << "Every year in " << MadLib[0] << ", people celebrate Irish heritage by ";

	cout << MadLib[1] << " the color " << MadLib[2] << ". ";

	cout << "Some people wear " << MadLib[3] << " hats with a " << MadLib[4];

	cout << " on them. Others wear " << MadLib[5] << " " << MadLib[6] << " ties, or ";

	cout << MadLib[7] << " colored " << MadLib[8] << " " << MadLib[9] << "." << " and pins that say \"" << MadLib[10];

	cout << " me, Im Irish!\" " << "are popular. The color " << MadLib[11] << " is associated with St.Patricks Day partly because Irelands nickname is The " << MadLib[12] << " Isle, and also because of the \"";

	cout << MadLib[13] << " leaf " << MadLib[14] << ", a symbol of " << MadLib[15] << " luck.\n";

	

}